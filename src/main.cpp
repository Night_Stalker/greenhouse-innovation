#include <TFT_eSPI.h>
#include <SPI.h>
#include <FastLED.h>
#include "DHT.h"

#define Mois1 39     // Soil moisture analog pin
#define Mois2 36     // Soil moisture analog pin
#define LightSen 34  // Light sensor analog pin
#define LightPWM 14  // LightPWM  change light color pin
#define Temp1 35     // DHT1 pin
#define Temp2 32     // DHT2 pin
// relay pins
#define Light 27       // Light on/off digital pin
#define Irrigation 26  // Irrigation digital pin
#define Fan1 12        // Fan1 on/off digital pin
#define Fan2 13        // Fan2 on/off digital pin

#define NUM_LEDS 51
CRGB leds[NUM_LEDS];
#define DHTTYPE DHT11
DHT DHT1(Temp1, DHTTYPE);
DHT DHT2(Temp2, DHTTYPE);

TFT_eSPI tft = TFT_eSPI();

#define SCREEN_HEIGHT 320
#define SCREEN_WIDTH 480
#define TREND_BOX_X1 40
#define TREND_BOX_Y1 45
#define TREND_BOX_HEIGHT 200
#define TREND_BOX_WIDTH 400
#define LOCAL_TEMPERATURE_MAX 50
#define LOCAL_TEMPERATURE_MIN -30
#define LOCAL_TEMPERATURE_COLOUR TFT_CYAN
#define LOCAL_TEMPERATURE_X 81
#define LOCAL_TEMPERATURE_Y 14
#define LOCAL_HUMIDITY_MAX 100
#define LOCAL_HUMIDITY_MIN 0
#define LOCAL_HUMIDITY_COLOUR TFT_PINK
#define LOCAL_HUMIDITY_X 83
#define LOCAL_HUMIDITY_Y 27
#define REMOTE_TEMPERATURE_MAX 50
#define REMOTE_TEMPERATURE_MIN -30
#define REMOTE_TEMPERATURE_COLOUR TFT_ORANGE
#define REMOTE_TEMPERATURE_X 280
#define REMOTE_TEMPERATURE_Y 14

#define ALARM_X 2
#define ALARM_Y 280

#define DATA_POINTS 24  // Number of data points for 24 hours

const char *fan, *light, *irrigation;
int night_day = 0;
void checkMoisture(void);
void checkLightIntensity(void);
void relayOnOff(char *name, int status);
void checkDHT(void);

// void setupFinalDisplay(void);
// void plotGraph(float data[], int dataCount, int yMin, int yMax, uint16_t color);
// void updateEnvironment(float newInsideTemperature, float newInsideHumidity, float newOutsideTemperature);
void setup(void) {
  Serial.begin(115200); /* Set the baudrate to 115200*/

  setupFinalDisplay();
  pinMode(Light, OUTPUT);
  pinMode(Temp1, OUTPUT);
  pinMode(Temp2, OUTPUT);
  pinMode(Fan1, OUTPUT);
  pinMode(Fan2, OUTPUT);
  pinMode(Irrigation, OUTPUT);
  FastLED.addLeds<WS2812B, LightPWM, GRB>(leds, NUM_LEDS);
}

void loop(void) {
  // analogSensorInput();

  delay(1000); /* Wait for 1000mS */

  // Show the LEDs
}

void checkDHT11(void) {
  int hum1 = DHT1.readHumidity();
  int temp1 = DHT1.readTemperature();
  int hum2 = DHT2.readHumidity();
  int temp2 = DHT2.readTemperature();

  if (isnan(hum1) || isnan(temp1)) {
    Serial.println("Failed to read from DHT1 sensor!");
  }
  if (isnan(hum2) || isnan(temp2)) {
    Serial.println("Failed to read from DHT2 sensor!");
  }
  int temp = (temp1 + temp2) / 2;
  int hum = (hum1 + hum2) / 2;
  if (temp > 28) {
    relayOnOff(fan, 1);
  } else if (temp < 23) {
    relayOnOff(fan, 0);
  }
}

void checkMoisture(void) {
  int moisture1 = (100 - ((analogRead(Mois1) / 4095.00) * 100));
  int moisture2 = (100 - ((analogRead(Mois2) / 4095.00) * 100));
  int mousture = (moisture1 + moisture2) / 2;
  if (night_day == 1 & (mousture < 80)) {  // if day
    relayOnOff(irrigation, 1);
  } else if (night_day == 1 & (mousture > 90)) {
    relayOnOff(irrigation, 0);
  } else if (night_day == 0 & (mousture < 65)) {  // if night
    relayOnOff(irrigation, 1);
  } else if (night_day == 0 & (mousture < 75)) {
    relayOnOff(irrigation, 0);
  }
  Serial.print("Moisture1 = ");
  Serial.print(moisture1);
  Serial.println("%");
  Serial.print("Moisture2 = ");
  Serial.print(moisture2);
  Serial.println("%");
}

void checkLightIntensity(void) {
  // leds[i] = CRGB(0, 204, 204);  // 1
  // leds[i] = CRGB(0, 32, 153);  // 2
  // leds[i] = CRGB(102, 204, 0);  // 3
  int light_intensity = analogRead(LightSen);
  if (light_intensity < 2000) {
    for (int i = 0; i < NUM_LEDS; i++) {
      leds[i] = CRGB(0, 204, 204);  // Songogdono
    }
    night_day = 0;  // Night
    relayOnOff(light, 1);
    delay(20);
    FastLED.show();
    Serial.println(" => Dim");
  } else if (light_intensity > 2500) {
    night_day = 1;  // Day
    Serial.println(" => Bright");
    relayOnOff(light, 0);
    delay(20);
  }
}

void relayOnOff(char *name, int status) {
  if (strcpy("fan1", name) == 0) {
    if (status == 1) {
      Serial.println("Fan1 On");
      digitalWrite(Fan1, 1);
      digitalWrite(Fan2, 1);
    } else {
      Serial.println("Fan Off");
      digitalWrite(Fan1, 0);
      digitalWrite(Fan2, 0);
    }
  } else if (strcpy("light", name) == 0) {
    if (status == 1) {
      Serial.println("Light On");
      digitalWrite(Light, 1);
    } else {
      Serial.println("Light Off");
      digitalWrite(Light, 0);
    }
  } else if (strcpy("irrigation", name) == 0) {
    if (status == 1) {
      Serial.println("Irrigation On");
      digitalWrite(Irrigation, 1);
    } else {
      Serial.println("Irrigation Off");
      digitalWrite(Irrigation, 0);
    }
  }
}

void setupFinalDisplay(void) {
  tft.fillScreen(TFT_BLACK);

  tft.setTextFont(2);
  tft.setRotation(0);
  tft.setCursor(100, 2);
  tft.print("Temperature Degrees C");
  tft.setCursor(110, 460);
  tft.print("Relative Humidity %");

  tft.setTextSize(1);
  tft.setRotation(1);
  tft.drawRect(TREND_BOX_X1, TREND_BOX_Y1, TREND_BOX_WIDTH, TREND_BOX_HEIGHT, TFT_LIGHTGREY);
  tft.drawRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, TFT_LIGHTGREY);

  // Add Temperature Scale values
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(TREND_BOX_X1 - 17, TREND_BOX_Y1 - 0);
  tft.print("50");
  tft.setCursor(TREND_BOX_X1 - 12, TREND_BOX_Y1 + 114);
  tft.print("0");
  tft.setCursor(TREND_BOX_X1 - 23, TREND_BOX_Y1 + TREND_BOX_HEIGHT - 15);
  tft.print("-30");

  // Add Humidity Scale values
  tft.setTextColor(TFT_WHITE);
  tft.setCursor(TREND_BOX_X1 + TREND_BOX_WIDTH + 5, TREND_BOX_Y1 - 0);
  tft.print("100");
  tft.setCursor(TREND_BOX_X1 + TREND_BOX_WIDTH + 5, TREND_BOX_Y1 + TREND_BOX_HEIGHT - 6);
  tft.print("0");

  // Draw static labels
  tft.setCursor(2, 2);
  tft.print("Air");
  tft.drawFastHLine(2, 16, 35, TFT_WHITE);
  tft.setTextColor(LOCAL_TEMPERATURE_COLOUR);
  tft.setCursor(2, 15);
  tft.print("Temperature");
  tft.setCursor(LOCAL_TEMPERATURE_X, LOCAL_TEMPERATURE_Y);
  tft.print("00.00");
  tft.setTextColor(LOCAL_HUMIDITY_COLOUR);
  tft.setCursor(2, 27);
  tft.print("Humidity");
  tft.setCursor(LOCAL_HUMIDITY_X, LOCAL_HUMIDITY_Y);
  tft.print("00.00");

  tft.setTextColor(TFT_WHITE);
  tft.setCursor(165, 2);
  tft.print("Soil");
  tft.drawFastHLine(165, 16, 45, TFT_WHITE);
  tft.setTextColor(REMOTE_TEMPERATURE_COLOUR);
  tft.setCursor(165, 15);
  tft.print("Moisture");
  tft.setCursor(REMOTE_TEMPERATURE_X, REMOTE_TEMPERATURE_Y);
  tft.print("00.00");

  tft.setTextColor(TFT_WHITE);
  tft.setCursor(325, 2);
  tft.print("AUTO");

  tft.setTextColor(TFT_RED);
  tft.setCursor(ALARM_X, ALARM_Y);
  tft.print("ALARM_1: ");
  tft.setCursor(ALARM_X + 70, ALARM_Y);
  tft.print("OFF");
  tft.setCursor(ALARM_X + 160, ALARM_Y);
  tft.print("ALARM_2: ");
  tft.setCursor(ALARM_X + 230, ALARM_Y);
  tft.print("OFF");
  tft.setCursor(ALARM_X + 320, ALARM_Y);
  tft.print("ALARM_3: ");
  tft.setCursor(ALARM_X + 390, ALARM_Y);
  tft.print("OFF");
}

void updateEnvironment(float newInsideTemperature, float newInsideHumidity, float newOutsideTemperature) {
  if (newInsideTemperature == 0 | newInsideHumidity == 0 | newOutsideTemperature == 0) {
    return;
  }
  // Shift all data points one position to the left for inside temperature
  for (int i = 1; i < DATA_POINTS; i++) {
    temperatureData[i - 1] = temperatureData[i];
  }
  // Add the new inside temperature value at the end
  temperatureData[DATA_POINTS - 1] = newInsideTemperature;

  // Shift all data points one position to the left for inside humidity
  for (int i = 1; i < DATA_POINTS; i++) {
    humidityData[i - 1] = humidityData[i];
  }
  // Add the new inside humidity value at the end
  humidityData[DATA_POINTS - 1] = newInsideHumidity;

  // Plot the updated graphs for inside temperature, inside humidity, and outside temperature
  plotGraph(temperatureData, humidityData, outTempData, DATA_POINTS, LOCAL_TEMPERATURE_MIN, LOCAL_TEMPERATURE_MAX, LOCAL_HUMIDITY_MIN, LOCAL_HUMIDITY_MAX, REMOTE_TEMPERATURE_MIN, REMOTE_TEMPERATURE_MAX, LOCAL_TEMPERATURE_COLOUR, LOCAL_HUMIDITY_COLOUR, REMOTE_TEMPERATURE_COLOUR);

  // Update the displayed temperature values
  tft.fillRect(LOCAL_TEMPERATURE_X, LOCAL_TEMPERATURE_Y, 39, 18, TFT_BLACK);  // Clear previous inside temperature display
  tft.setCursor(LOCAL_TEMPERATURE_X, LOCAL_TEMPERATURE_Y);
  tft.setTextColor(LOCAL_TEMPERATURE_COLOUR);
  tft.print(newInsideTemperature, 2);

  tft.fillRect(LOCAL_HUMIDITY_X, LOCAL_HUMIDITY_Y, 39, 18, TFT_BLACK);  // Clear previous inside humidity display
  tft.setCursor(LOCAL_HUMIDITY_X, LOCAL_HUMIDITY_Y);
  tft.setTextColor(LOCAL_HUMIDITY_COLOUR);
  tft.print(newInsideHumidity, 2);

  tft.fillRect(REMOTE_TEMPERATURE_X, REMOTE_TEMPERATURE_Y, 39, 18, TFT_BLACK);  // Clear previous outside temperature display
  tft.setCursor(REMOTE_TEMPERATURE_X, REMOTE_TEMPERATURE_Y);
  tft.setTextColor(REMOTE_TEMPERATURE_COLOUR);
  tft.print(newOutsideTemperature, 2);
}

void plotGraph(float insideTemperatureData[], float insideHumidityData[], float outsideTemperatureData[], int dataCount, int yMinTemp, int yMaxTemp, int yMinHumid, int yMaxHumid, int yMinOutsideTemp, int yMaxOutsideTemp, uint16_t tempColor, uint16_t humidColor, uint16_t outsideTempColor) {
  tft.fillRect(TREND_BOX_X1, TREND_BOX_Y1, TREND_BOX_WIDTH, TREND_BOX_HEIGHT, TFT_BLACK);      // Clear previous graph
  tft.drawRect(TREND_BOX_X1, TREND_BOX_Y1, TREND_BOX_WIDTH, TREND_BOX_HEIGHT, TFT_LIGHTGREY);  // Redraw border

  // Plot inside temperature
  int prevX = TREND_BOX_X1;
  int prevY = map(insideTemperatureData[0], yMinTemp, yMaxTemp, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

  for (int i = 1; i < dataCount; i++) {
    int x = TREND_BOX_X1 + (i * (TREND_BOX_WIDTH / (dataCount - 1)));
    int y = map(insideTemperatureData[i], yMinTemp, yMaxTemp, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

    tft.drawLine(prevX, prevY, x, y, tempColor);

    prevX = x;
    prevY = y;
  }

  // Plot inside humidity
  prevX = TREND_BOX_X1;
  prevY = map(insideHumidityData[0], yMinHumid, yMaxHumid, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

  for (int i = 1; i < dataCount; i++) {
    int x = TREND_BOX_X1 + (i * (TREND_BOX_WIDTH / (dataCount - 1)));
    int y = map(insideHumidityData[i], yMinHumid, yMaxHumid, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

    tft.drawLine(prevX, prevY, x, y, humidColor);

    prevX = x;
    prevY = y;
  }

  // Plot outside temperature
  prevX = TREND_BOX_X1;
  prevY = map(outsideTemperatureData[0], yMinOutsideTemp, yMaxOutsideTemp, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

  for (int i = 1; i < dataCount; i++) {
    int x = TREND_BOX_X1 + (i * (TREND_BOX_WIDTH / (dataCount - 1)));
    int y = map(outsideTemperatureData[i], yMinOutsideTemp, yMaxOutsideTemp, TREND_BOX_Y1 + TREND_BOX_HEIGHT, TREND_BOX_Y1);

    tft.drawLine(prevX, prevY, x, y, outsideTempColor);

    prevX = x;
    prevY = y;
  }
}